# Jenkins
TBA

## Operations
### pve-vm-create
| Parameter | Default Value | Comments |
| --- | --- | --- |
||||

### pve-vm-registration
| Parameter | Default Value | Comments |
| --- | --- | --- |
||||

### pve-vm-resignation
| Parameter | Default Value | Comments |
| --- | --- | --- |
||||

### pve-vm-destroy
| Parameter | Default Value | Comments |
| --- | --- | --- |
||||